import tensorflow as tf
print(tf.__version__)
mnist = tf.keras.datasets.fashion_mnist
(training_images, training_labels), (test_images, test_labels) = mnist.load_data()
import matplotlib.pyplot as plt
plt.imshow(training_images[0])
print(training_labels[0])
print(training_images[0])
model = tf.keras.models.Sequential([tf.keras.layers.Flatten(), 
tf.keras.layers.Dense(1024, activation=tf.nn.relu), 
tf.keras.layers.Dense(10, activation=tf.nn.softmax)])
model.compile(optimizer = 'adam',
           loss = 'sparse_categorical_crossentropy',
           metrics=['accuracy'])

model.fit(training_images, training_labels, epochs=5)
test_loss, test_acc = model.evaluate(test_images, test_labels)
print("evaluation", test_loss, test_acc)
#predictions = model.predict(my_images)
classifications = model.predict(test_images)
print(classifications[0])
print(test_labels[0])